<?php

namespace Code20\DepotBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Code20\DepotBundle\Entity\Product;
use Code20\DepotBundle\Form\Product\ProductType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class OrdersController extends Controller
{
    public function indexAction()
    {
        $orders = $this->getDoctrine()->getManager()->getRepository('Code20DepotBundle:OrderEntry')->findAll();
        return $this->render('Code20DepotBundle:Orders:index.html.twig', array(
            'orders' => $orders
        ));
    }
    
}
