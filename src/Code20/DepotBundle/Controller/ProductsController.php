<?php

namespace Code20\DepotBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Code20\DepotBundle\Entity\Product;
use Code20\DepotBundle\Form\Product\ProductType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class ProductsController extends Controller
{
    public function indexAction()
    {
        $products = $this->getDoctrine()->getManager()->getRepository('Code20DepotBundle:Product')->findAll();
        
        return $this->render('Code20DepotBundle:Products:index.html.twig', array(
            'products' => $products
        ));
    }
    
    public function newAction(Request $request)
    {
        $product = new Product();
        $form = $this->createForm(new ProductType(), $product);
        
        $form->handleRequest($request);
        
        if ($form->isValid())
        {
            $em = $this->getDoctrine()->getManager();
            $em->persist($product);
            $em->flush();
            
            $this->get('session')->getFlashBag()->add(
                'success',
                'New product added!'
            );
            
            return $this->redirect($this->generateUrl('admin_products'));
        }
        
        return $this->render('Code20DepotBundle:Products:new.html.twig', array(
            'form' => $form->createView()
        ));
    }
    
    public function editAction(Request $request, $id)
    {
        $product = $this->getDoctrine()->getManager()
                ->getRepository('Code20DepotBundle:Product')->findOneById($id);
        
        if ($product == NULL)
        {
            throw new AccessDeniedHttpException();
        }
        
        $form = $this->createForm(new ProductType(), $product);
        
        $form->handleRequest($request);
        
        if ($form->isValid())
        {
            $em = $this->getDoctrine()->getManager();
            $em->flush();
            
            $this->get('session')->getFlashBag()->add(
                'success',
                'Product changed'
            );
            
            return $this->redirect($this->generateUrl('admin_products'));
        }
        
        return $this->render('Code20DepotBundle:Products:edit.html.twig', array(
            'form' => $form->createView()
        ));
    }
    
    public function deleteAction(Request $request, $id)
    {
        $product = $this->getDoctrine()->getManager()
                ->getRepository('Code20DepotBundle:Product')->findOneById($id);
        
        if ($product == NULL)
        {
            throw new AccessDeniedHttpException();
        }
        
        $em = $this->getDoctrine()->getManager();
        $em->remove($product);
        $em->flush();
            
        $this->get('session')->getFlashBag()->add(
            'success',
            'Product deleted'
        );
        
        return $this->redirect($this->generateUrl('admin_products'));
    }
}
