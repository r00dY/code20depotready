<?php

namespace Code20\DepotBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Code20\DepotBundle\Entity\Cart;
use Code20\DepotBundle\Entity\LineItem;
use Code20\DepotBundle\Entity\OrderEntry;

use Code20\DepotBundle\Form\OrderEntry\OrderEntryType;

class ShopController extends Controller
{
    public function indexAction(Request $request)
    {
        return $this->render('Code20DepotBundle:Shop:index.html.twig');
    }
    
    public function aboutUsAction()
    {
        return $this->render('Code20DepotBundle:Shop:aboutUs.html.twig');
    }
    
    public function productsAction(Request $request)
    {
        $products = $this->getDoctrine()->getManager()->getRepository('Code20DepotBundle:Product')->findAll();

        return $this->render('Code20DepotBundle:Shop:products.html.twig', [
            'products' => $products
        ]);
    }
    
    private function getCurrentCart()
    {
        $session = $this->get('session');
        if ($session->has('cart_id'))
        {
            $cart = $this->getDoctrine()->getManager()
                    ->getRepository('Code20DepotBundle:Cart')->find($session->get('cart_id'));
            
            return $cart;
        }
        
        return NULL;
    }
    
    public function cartAction(Request $request)
    {
        $cart = $this->getCurrentCart();
        
        if ($cart)
        {
            $lineItems = $cart->getLineItems();
        }
        else
        {
            $lineItems = [];
        }
        
        return $this->render('Code20DepotBundle:Shop:cart.html.twig', [
            'lineItems' => $lineItems
        ]);
    }
    
    public function cartDeleteLineItemAction(Request $request, $lineItemId)
    {
        $em = $this->getDoctrine()->getManager();
        
        $cart = $this->getCurrentCart();
        if (!$cart)
        {
            throw new AccessDeniedHttpException();
        }
        
        $currentLineItem = null;
        foreach($cart->getLineItems() as $lineItem)
        {
            if ($lineItem->getId() == $lineItemId)
            {
                $currentLineItem = $lineItem;
                break;
            }
        }
        
        if (!$currentLineItem)
        {
            throw new AccessDeniedHttpException();
        }
        
        $em->remove($currentLineItem);
        $em->flush();
        
        $this->get('session')->getFlashBag()->add(
            'success',
            'Product removed from cart'
        );
        
        return $this->redirect($this->generateUrl('cart'));
    }
    
    public function cartAddLineItemAction(Request $request, $productId)
    {
        $em = $this->getDoctrine()->getManager();
        
        $product = $em->getRepository('Code20DepotBundle:Product')->find($productId);
        if (!$product)
        {
            throw new AccessDeniedHttpException();
        }
        
        // Obtain cart
        $session = $this->get('session');
        if ($session->has('cart_id'))
        {
            $cart = $em->getRepository('Code20DepotBundle:Cart')->find($session->get('cart_id'));
            if (!$cart)
            {
                $session->remove('cart_id');
            }
        }
        if (!$session->has('cart_id'))
        {
            $cart = new Cart();
            $em->persist($cart);
            $em->flush($cart);
            $session->set('cart_id', $cart->getId());
        }
        
        // Add product to cart
        $lineItems = $cart->getLineItems();
        $currentLineItem = NULL;
        foreach($lineItems as $lineItem)
        {
            if ($lineItem->getProduct() == $product)
            {
                $currentLineItem = $lineItem;
                break;
            }
        }
        
        if ($currentLineItem)
        {
            $currentLineItem->setQuantity($currentLineItem->getQuantity() + 1);
        }
        else
        {
            $currentLineItem = new LineItem();
            $currentLineItem->setQuantity(1);
            $currentLineItem->setCart($cart);
            $currentLineItem->setProduct($product);
            $em->persist($currentLineItem);
        }
        
        $em->flush();
        
        $this->get('session')->getFlashBag()->add(
            'success',
            'Product added to cart'
        );
        
        return $this->redirect($this->generateUrl('cart'));
    }
    
    public function cartOrderAction(Request $request)
    {
        $cart = $this->getCurrentCart();
        if (!$cart)
        {
            throw new AccessDeniedHttpException();
        }
        
        $orderEntry = new OrderEntry();
        $form = $this->createForm(new OrderEntryType(), $orderEntry);
        
        $form->handleRequest($request);
        
        if ($form->isValid())
        {
            $em = $this->getDoctrine()->getManager();
            $em->persist($orderEntry);
            $em->flush();
            
            $this->get('session')->getFlashBag()->add(
                'success',
                'Order done!'
            );
            
            $this->get('session')->remove('cart_id');
            
            return $this->redirect($this->generateUrl('products'));
        }
        
        return $this->render('Code20DepotBundle:Shop:order.html.twig', [
            'form' => $form->createView(),
            'lineItems' => $cart->getLineItems()
        ]);
    }
}
