<?php

namespace Code20\DepotBundle\Form\OrderEntry;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class OrderEntryType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('surname')
            ->add('email')
            ->add('phoneNumber')
            ->add('street')
            ->add('buildingNumber')
            ->add('flatNumber')
            ->add('postalCode')
            ->add('city')
            ->add('order', 'submit');
    }

    public function getName()
    {
        return 'orderEntry';
    }
}
