<?php

namespace Code20\DepotBundle\Form\Product;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class ProductType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('description')
            ->add('price', 'integer')
            ->add('save', 'submit');
    }

    public function getName()
    {
        return 'product';
    }
}
