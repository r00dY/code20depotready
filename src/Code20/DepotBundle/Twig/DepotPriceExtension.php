<?php

namespace Code20\DepotBundle\Twig;

class DepotPriceExtension extends \Twig_Extension
{
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('depot_price', array($this, 'depotPriceFilter')),
        );
    }

    public function depotPriceFilter($number)
    {
        $gr = $number % 100;
        $zl = floor($number / 100);
        
        if ($zl == 0)
        {
            return "{$number}gr";
        }
        
        $result = "{$zl}zł";
        if ($gr != 0)
        {
            $result .= " {$gr}gr";
        }

        return $result;
    }

    public function getName()
    {
        return 'depot_price';
    }
}